# Web Development assignment for Hdip using html and css#

![watersports.JPG](https://bitbucket.org/repo/xoAKXb/images/3123172022-watersports.JPG)

[Live site here](http://austincunningham.bitbucket.org)

### Project One: General Requirements ###

* Consistency across pages
* Clear and consistent file naming scheme
* Clear and consistent site structure (e.g., folders)
* Clear and consistent code nesting and indentation
* Use HTML tags introduced during the class + elements you think make sense in the context of the site you are developing
* Multi-columns layout
* Use a combination of text and images, where appropriate
* Use of classes and ID + suitable HTML tags

You can access this repo with SSH or with HTTPS.